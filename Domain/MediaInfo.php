<?php

namespace MindGeek\MediaInfoBundle\Domain;

use \MindGeek\MediaInfoBundle\Domain\MediaInfo\AudioInfo;
use \MindGeek\MediaInfoBundle\Domain\MediaInfo\GeneralInfo;
use \MindGeek\MediaInfoBundle\Domain\MediaInfo\VideoInfo;

class MediaInfo extends FileInfo
{

	/**
	 * @var GeneralInfo
	 */
	protected $generalInfo;

	/**
	 * @var VideoInfo
	 */
	protected $videoInfo;

	/**
	 * @var AudioInfo
	 */
	protected $audioInfo;

	/**
	 * @param \MindGeek\MediaInfoBundle\Domain\MediaInfo\AudioInfo $audioInfo
	 *
	 * @return $this
	 */
	public function setAudioInfo(AudioInfo $audioInfo)
	{
		$this->audioInfo = $audioInfo;

		return $this;
	}

	/**
	 * @return \MindGeek\MediaInfoBundle\Domain\MediaInfo\AudioInfo
	 */
	public function getAudioInfo()
	{
		return $this->audioInfo;
	}

	/**
	 * @param \MindGeek\MediaInfoBundle\Domain\MediaInfo\GeneralInfo $generalInfo
	 *
	 * @return $this
	 */
	public function setGeneralInfo(GeneralInfo $generalInfo)
	{
		$this->generalInfo = $generalInfo;

		return $this;
	}

	/**
	 * @return \MindGeek\MediaInfoBundle\Domain\MediaInfo\GeneralInfo
	 */
	public function getGeneralInfo()
	{
		return $this->generalInfo;
	}

	/**
	 * @param \MindGeek\MediaInfoBundle\Domain\MediaInfo\VideoInfo $videoInfo
	 *
	 * @return $this
	 */
	public function setVideoInfo(VideoInfo $videoInfo)
	{
		$this->videoInfo = $videoInfo;

		return $this;
	}

	/**
	 * @return \MindGeek\MediaInfoBundle\Domain\MediaInfo\VideoInfo
	 */
	public function getVideoInfo()
	{
		return $this->videoInfo;
	}

} 