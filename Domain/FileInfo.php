<?php

namespace MindGeek\MediaInfoBundle\Domain;

class FileInfo
{

	/**
	 * @var string
	 */
	protected $fileName = '';

	/**
	 * @var int
	 */
	protected $fileSize = 0;

	/**
	 * @var string
	 */
	protected $fileExtension = '';

	/**
	 * @param string $fileName
	 *
	 * @return $this
	 */
	public function setFileName($fileName)
	{
		$this->fileName = $fileName;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getFileName()
	{
		return $this->fileName;
	}

	/**
	 * @param int $size
	 *
	 * @return $this
	 */
	public function setFileSize($size)
	{
		$this->fileSize = (int) $size;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getFileSize()
	{
		return $this->fileSize;
	}

	/**
	 * @param string $extension
	 *
	 * @return $this
	 */
	public function setFileExtension($extension)
	{
		$this->fileExtension = $extension;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getFileExtension()
	{
		return $this->fileExtension;
	}

}