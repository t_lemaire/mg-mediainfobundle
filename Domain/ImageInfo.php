<?php

namespace MindGeek\MediaInfoBundle\Domain;

class ImageInfo extends FileInfo
{

	/**
	 * @var int
	 */
	protected $width = 0;

	/**
	 * @var int
	 */
	protected $height = 0;

	/**
	 * @var string
	 */
	protected $mime = '';

	/**
	 * @var int
	 */
	protected $bits = 0;

	/**
	 * @param string $mime
	 *
	 * @return $this
	 */
	public function setMime($mime)
	{
		$this->mime = $mime;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getMime()
	{
		return $this->mime;
	}

	/**
	 * @param int $height
	 *
	 * @return $this
	 */
	public function setHeight($height)
	{
		$this->height = (int) $height;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getHeight()
	{
		return $this->height;
	}

	/**
	 * @param int $width
	 *
	 * @return $this
	 */
	public function setWidth($width)
	{
		$this->width = (int) $width;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getWidth()
	{
		return $this->width;
	}

	/**
	 * @param int $bits
	 *
	 * @return $this
	 */
	public function setBits($bits)
	{
		$this->bits = (int) $bits;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getBits()
	{
		return $this->bits;
	}

}