<?php

namespace MindGeek\MediaInfoBundle\Domain\MediaInfo;

class VideoInfo
{

    /**
     * @var string
     */
    protected $format = '';

    /**
     * @var string
     */
    protected $formatInfo = '';

    /**
     * @var string
     */
    protected $formatProfile = '';

    /**
     * @var string
     */
    protected $formatSettingsCABAC = '';

    /**
     * @var string
     */
    protected $formatSettingsReFrames = '';

    /**
     * @var string
     */
    protected $codecId = '';

    /**
     * @var string
     */
    protected $codecIdInfo = '';

    /**
     * @var int
     */
    protected $duration = 0;

    /**
     * @var int
     */
    protected $bitRate = '';

    /**
     * @var int
     */
    protected $maximumBitRate = '';

    /**
     * @var int
     */
    protected $width = 0;

    /**
     * @var int
     */
    protected $height = 0;

    /**
     * @var string
     */
    protected $displayAspectRatio = '';

    /**
     * @var string
     */
    protected $originalDisplayAspectRatio = '';

    /**
     * @var string
     */
    protected $frameRateMode = '';

    /**
     * @var string
     */
    protected $frameRate = 0;

    /**
     * @var string
     */
    protected $colorSpace = '';

    /**
     * @var string
     */
    protected $chromaSubSampling = '';

    /**
     * @var string
     */
    protected $bitDepth = 0;

    /**
     * @var string
     */
    protected $scanType = '';

    /**
     * @var string
     */
    protected $bitsPixelFrame = 0;

    /**
     * @var int
     */
    protected $streamSize = 0;

    /**
     * @param string $bitDepth
     *
     * @return $this
     */
    public function setBitDepth($bitDepth)
    {
        $this->bitDepth = $bitDepth;

        return $this;
    }

    /**
     * @return string
     */
    public function getBitDepth()
    {
        return $this->bitDepth;
    }

    /**
     * @param int $bitRate
     *
     * @return $this
     */
    public function setBitRate($bitRate)
    {
        $this->bitRate = (int) $bitRate;

        return $this;
    }

    /**
     * @return int
     */
    public function getBitRate()
    {
        return $this->bitRate;
    }

    /**
     * @param string $bitsPixelFrame
     *
     * @return $this
     */
    public function setBitsPixelFrame($bitsPixelFrame)
    {
        $this->bitsPixelFrame = $bitsPixelFrame;

        return $this;
    }

    /**
     * @return string
     */
    public function getBitsPixelFrame()
    {
        return $this->bitsPixelFrame;
    }

    /**
     * @param string $chromaSubSampling
     *
     * @return $this
     */
    public function setChromaSubSampling($chromaSubSampling)
    {
        $this->chromaSubSampling = $chromaSubSampling;

        return $this;
    }

    /**
     * @return string
     */
    public function getChromaSubSampling()
    {
        return $this->chromaSubSampling;
    }

    /**
     * @param string $codecId
     *
     * @return $this
     */
    public function setCodecId($codecId)
    {
        $this->codecId = $codecId;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodecId()
    {
        return $this->codecId;
    }

    /**
     * @param string $codecIdInfo
     *
     * @return $this
     */
    public function setCodecIdInfo($codecIdInfo)
    {
        $this->codecIdInfo = $codecIdInfo;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodecIdInfo()
    {
        return $this->codecIdInfo;
    }

    /**
     * @param string $colorSpace
     *
     * @return $this
     */
    public function setColorSpace($colorSpace)
    {
        $this->colorSpace = $colorSpace;

        return $this;
    }

    /**
     * @return string
     */
    public function getColorSpace()
    {
        return $this->colorSpace;
    }

    /**
     * @param string $displayAspectRatio
     *
     * @return $this
     */
    public function setDisplayAspectRatio($displayAspectRatio)
    {
        $this->displayAspectRatio = $displayAspectRatio;

        return $this;
    }

    /**
     * @return string
     */
    public function getDisplayAspectRatio()
    {
        return $this->displayAspectRatio;
    }

    /**
     * @param string $originalDisplayAspectRatio
     *
     * @return $this
     */
    public function setOriginalDisplayAspectRatio($originalDisplayAspectRatio)
    {
        $this->originalDisplayAspectRatio = $originalDisplayAspectRatio;

        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalDisplayAspectRatio()
    {
        return $this->originalDisplayAspectRatio;
    }

    /**
     * @param int $duration
     *
     * @return $this
     */
    public function setDuration($duration)
    {
        $this->duration = (int) $duration;

        return $this;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param string $format
     *
     * @return $this
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $formatInfo
     *
     * @return $this
     */
    public function setFormatInfo($formatInfo)
    {
        $this->formatInfo = $formatInfo;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormatInfo()
    {
        return $this->formatInfo;
    }

    /**
     * @param string $formatProfile
     *
     * @return $this
     */
    public function setFormatProfile($formatProfile)
    {
        $this->formatProfile = $formatProfile;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormatProfile()
    {
        return $this->formatProfile;
    }

    /**
     * @param string $formatSettingsCABAC
     *
     * @return $this
     */
    public function setFormatSettingsCABAC($formatSettingsCABAC)
    {
        $this->formatSettingsCABAC = $formatSettingsCABAC;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormatSettingsCABAC()
    {
        return $this->formatSettingsCABAC;
    }

    /**
     * @param string $formatSettingsReFrames
     *
     * @return $this
     */
    public function setFormatSettingsReFrames($formatSettingsReFrames)
    {
        $this->formatSettingsReFrames = $formatSettingsReFrames;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormatSettingsReFrames()
    {
        return $this->formatSettingsReFrames;
    }

    /**
     * @param string $frameRate
     *
     * @return $this
     */
    public function setFrameRate($frameRate)
    {
        $this->frameRate = $frameRate;

        return $this;
    }

    /**
     * @return string
     */
    public function getFrameRate()
    {
        return $this->frameRate;
    }

    /**
     * @param string $frameRateMode
     *
     * @return $this
     */
    public function setFrameRateMode($frameRateMode)
    {
        $this->frameRateMode = $frameRateMode;

        return $this;
    }

    /**
     * @return string
     */
    public function getFrameRateMode()
    {
        return $this->frameRateMode;
    }

    /**
     * @param int $height
     *
     * @return $this
     */
    public function setHeight($height)
    {
        $this->height = (int) $height;

        return $this;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $bitRate
     *
     * @return $this
     */
    public function setMaximumBitRate($bitRate)
    {
        $this->maximumBitRate = (int) $bitRate;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaximumBitRate()
    {
        return $this->maximumBitRate;
    }

    /**
     * @param string $scanType
     *
     * @return $this
     */
    public function setScanType($scanType)
    {
        $this->scanType = $scanType;

        return $this;
    }

    /**
     * @return string
     */
    public function getScanType()
    {
        return $this->scanType;
    }

    /**
     * @param int $streamSize
     *
     * @return $this
     */
    public function setStreamSize($streamSize)
    {
        $this->streamSize = (int) $streamSize;

        return $this;
    }

    /**
     * @return int
     */
    public function getStreamSize()
    {
        return $this->streamSize;
    }

    /**
     * @param int $width
     *
     * @return $this
     */
    public function setWidth($width)
    {
        $this->width = (int) $width;

        return $this;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }


}