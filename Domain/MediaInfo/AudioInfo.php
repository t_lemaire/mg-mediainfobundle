<?php

namespace MindGeek\MediaInfoBundle\Domain\MediaInfo;

class AudioInfo
{

	/**
	 * @var string
	 */
	protected $id = '';

	/**
	 * @var string
	 */
	protected $format = '';

	/**
	 * @var string
	 */
	protected $formatInfo = '';

	/**
	 * @var string
	 */
	protected $formatProfile = '';

	/**
	 * @var string
	 */
	protected $codecId = '';

	/**
	 * @var int
	 */
	protected $duration = 0;

	/**
	 * @var string
	 */
	protected $bitRateMode = '';

	/**
	 * @var int
	 */
	protected $bitRate = '';

	/**
	 * @var int
	 */
	protected $maximumBitRate = '';

	/**
	 * @var string
	 */
	protected $channels = 0;

	/**
	 * @var string
	 */
	protected $channelPositions = 0;

	/**
	 * @var string
	 */
	protected $samplingRate = '';

	/**
	 * @var string
	 */
	protected $compressionMode = '';

	/**
	 * @var int
	 */
	protected $streamSize = 0;

	/**
	 * @param int $bitRate
	 *
	 * @return $this
	 */
	public function setBitRate($bitRate)
	{
		$this->bitRate = (int) $bitRate;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getBitRate()
	{
		return $this->bitRate;
	}

	/**
	 * @param string $bitRateMode
	 *
	 * @return $this
	 */
	public function setBitRateMode($bitRateMode)
	{
		$this->bitRateMode = $bitRateMode;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getBitRateMode()
	{
		return $this->bitRateMode;
	}

	/**
	 * @param string $channelPositions
	 *
	 * @return $this
	 */
	public function setChannelPositions($channelPositions)
	{
		$this->channelPositions = $channelPositions;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getChannelPositions()
	{
		return $this->channelPositions;
	}

	/**
	 * @param string $channels
	 *
	 * @return $this
	 */
	public function setChannels($channels)
	{
		$this->channels = $channels;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getChannels()
	{
		return $this->channels;
	}

	/**
	 * @param string $codedId
	 *
	 * @return $this
	 */
	public function setCodecId($codedId)
	{
		$this->codecId = $codedId;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getCodecId()
	{
		return $this->codecId;
	}

	/**
	 * @param string $compressionMode
	 *
	 * @return $this
	 */
	public function setCompressionMode($compressionMode)
	{
		$this->compressionMode = $compressionMode;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getCompressionMode()
	{
		return $this->compressionMode;
	}

	/**
	 * @param int $duration
	 *
	 * @return $this
	 */
	public function setDuration($duration)
	{
		$this->duration = (int) $duration;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getDuration()
	{
		return $this->duration;
	}

	/**
	 * @param string $format
	 *
	 * @return $this
	 */
	public function setFormat($format)
	{
		$this->format = $format;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getFormat()
	{
		return $this->format;
	}

	/**
	 * @param string $formatInfo
	 *
	 * @return $this
	 */
	public function setFormatInfo($formatInfo)
	{
		$this->formatInfo = $formatInfo;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getFormatInfo()
	{
		return $this->formatInfo;
	}

	/**
	 * @param string $formatProfile
	 *
	 * @return $this
	 */
	public function setFormatProfile($formatProfile)
	{
		$this->formatProfile = $formatProfile;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getFormatProfile()
	{
		return $this->formatProfile;
	}

	/**
	 * @param string $id
	 *
	 * @return $this
	 */
	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $maximumBitRate
	 *
	 * @return $this
	 */
	public function setMaximumBitRate($maximumBitRate)
	{
		$this->maximumBitRate = (int) $maximumBitRate;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getMaximumBitRate()
	{
		return $this->maximumBitRate;
	}

	/**
	 * @param string $samplingRate
	 *
	 * @return $this
	 */
	public function setSamplingRate($samplingRate)
	{
		$this->samplingRate = $samplingRate;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getSamplingRate()
	{
		return $this->samplingRate;
	}

	/**
	 * @param int $streamSize
	 *
	 * @return $this
	 */
	public function setStreamSize($streamSize)
	{
		$this->streamSize = (int) $streamSize;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getStreamSize()
	{
		return $this->streamSize;
	}

} 