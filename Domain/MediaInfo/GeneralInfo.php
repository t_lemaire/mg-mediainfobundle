<?php

namespace MindGeek\MediaInfoBundle\Domain\MediaInfo;

class GeneralInfo
{

	/**
	 * @var string
	 */
	protected $format = '';

	/**
	 * @var string
	 */
	protected $formatProfile = '';

	/**
	 * @var string
	 */
	protected $codecId = '';

	/**
	 * @var int
	 */
	protected $fileSize = 0;

	/**
	 * @var int
	 */
	protected $duration = 0;

	/**
	 * @var string
	 */
	protected $overallBitRateMode = '';

	/**
	 * @var int
	 */
	protected $overallBitRate = '';

	/**
	 * @var string
	 */
	protected $writingApplication = '';

	/**
	 * @param string $codecId
	 *
	 * @return $this
	 */
	public function setCodecId($codecId)
	{
		$this->codecId = $codecId;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getCodecId()
	{
		return $this->codecId;
	}

	/**
	 * @param int $duration
	 *
	 * @return $this
	 */
	public function setDuration($duration)
	{
		$this->duration = (int) $duration;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getDuration()
	{
		return $this->duration;
	}

	/**
	 * @param int $fileSize
	 *
	 * @return $this
	 */
	public function setFileSize($fileSize)
	{
		$this->fileSize = (int) $fileSize;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getFileSize()
	{
		return $this->fileSize;
	}

	/**
	 * @param string $format
	 *
	 * @return $this
	 */
	public function setFormat($format)
	{
		$this->format = $format;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getFormat()
	{
		return $this->format;
	}

	/**
	 * @param string $formatProfile
	 *
	 * @return $this
	 */
	public function setFormatProfile($formatProfile)
	{
		$this->formatProfile = $formatProfile;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getFormatProfile()
	{
		return $this->formatProfile;
	}

	/**
	 * @param int $overallBitRate
	 *
	 * @return $this
	 */
	public function setOverallBitRate($overallBitRate)
	{
		$this->overallBitRate = (int) $overallBitRate;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getOverallBitRate()
	{
		return $this->overallBitRate;
	}

	/**
	 * @param string $overallBitRateMode
	 *
	 * @return $this
	 */
	public function setOverallBitRateMode($overallBitRateMode)
	{
		$this->overallBitRateMode = $overallBitRateMode;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getOverallBitRateMode()
	{
		return $this->overallBitRateMode;
	}

	/**
	 * @param string $writingApplication
	 *
	 * @return $this
	 */
	public function setWritingApplication($writingApplication)
	{
		$this->writingApplication = $writingApplication;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getWritingApplication()
	{
		return $this->writingApplication;
	}

} 