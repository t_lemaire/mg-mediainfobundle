#!/bin/bash

# Usage instructions
USAGE="Usage: ./chk4hls.sh [OPTION]... [FILE]...
Detect HLS file header in

Description: Detect possibly  malicious HLS files masquerading as other video file types.
  Prior to ffmpeg version 2.8.5, rougue HLS files opened an attack allowing for root
  exploits. Version 2.8.5 and greater are not vulnerable to the explot, but do crash.
  bringing down their calling process.

  This script was written to be run against files prior to being used as an input in
  ffmpeg, ffprobe or ffplay.

Command-line aguments:
  -q                        suppress all normal output
  -s                        suppress grep output
  -h                        display this help and exit

With no FILE or when FILE is -, read standard input.

Exit Codes:
  0                         no HLS data found in file(s)
  1                         possible unsafe HLS data found in file(s)
  2                         error occurred (if -s is not given)
"

OPTIND=1

# Default flag values
QUIETOPT=false
SUPPRESSOPT=false

while getopts "sqh" o ; do # set $o to the next passed option
  case "$o" in
    q) # set QUIETOPT to true
       QUIETOPT=true
       SUPPRESSOPT=true
       ;;
    s) # set SUPRESSOPT to true
       SUPPRESSOPT=true
       ;;
    h) /usr/bin/printf "%s" "$USAGE"
       exit 0
       ;;
  esac
done

shift $(($OPTIND - 1))
# $1 is now the first non-option argument, $2 the second, etc

# Run grep command to look for HLS header (case-insensitive)
if [ $SUPPRESSOPT = true ]; then
  LC_ALL=C /bin/grep -F -q -a -i '#EXTM3U' $@
else
  /usr/bin/printf "%s\n" "Grep output:"
  LC_ALL=C /bin/grep -F -a -i '#EXTM3U' $@
  /usr/bin/printf "\n"
fi

# Simplify our exit codes, 0 if found, 1 for any other code.
rc=$?
if [ $rc -eq 0 ]; then
  if [ $QUIETOPT != true ]; then
    /usr/bin/printf "%s\n" "Result: HLS header detected, one or more file(s) is possiby malicious..."
  fi
  exit 1
elif [ $rc -eq 2 ]; then
  if [ $QUIETOPT != true ]; then
    /usr/bin/printf "%s\n" "Result: Runtime error."
  fi
  exit 2
else
  if [ $QUIETOPT != true ]; then
    /usr/bin/printf "%s\n" "Result: No HLS header detected, file(s) appear safe..."
  fi
  exit 0
fi
