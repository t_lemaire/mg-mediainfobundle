<?php

namespace MindGeek\MediaInfoBundle\Tests\Domain;

use \MindGeek\MediaInfoBundle\Domain\MediaInfo;

class MediaInfoTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingGeneralInfo()
	{
		$mediaInfo = new MediaInfo();

		$expected = new MediaInfo\GeneralInfo();

		$mediaInfo->setGeneralInfo($expected);
		$this->assertEquals($expected, $mediaInfo->getGeneralInfo());
	}

	public function testSettingAudioInfo()
	{
		$imageInfo = new MediaInfo();

		$expected = new MediaInfo\AudioInfo();

		$imageInfo->setAudioInfo($expected);
		$this->assertEquals($expected, $imageInfo->getAudioInfo());
	}


	public function testSettingVideoInfo()
	{
		$imageInfo = new MediaInfo();

		$expected = new MediaInfo\VideoInfo();

		$imageInfo->setVideoInfo($expected);
		$this->assertEquals($expected, $imageInfo->getVideoInfo());
	}

}