<?php

namespace MindGeek\MediaInfoBundle\Tests\Domain;

use \MindGeek\MediaInfoBundle\Domain\ImageInfo;

class ImageInfoTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingWidth()
	{
		$imageInfo = new ImageInfo();

		$expected = 100;

		$imageInfo->setWidth($expected);
		$this->assertEquals($expected, $imageInfo->getWidth());

		$imageInfo->setWidth('100px');
		$this->assertEquals($expected, $imageInfo->getWidth());
	}

	public function testSettingHeight()
	{
		$imageInfo = new ImageInfo();

		$expected = 100;

		$imageInfo->setHeight($expected);
		$this->assertEquals($expected, $imageInfo->getHeight());

		$imageInfo->setHeight('100px');
		$this->assertEquals($expected, $imageInfo->getHeight());
	}

	public function testSettingSize()
	{
		$fileInfo = new ImageInfo();

		$expected = 8;

		$fileInfo->setBits($expected);
		$this->assertEquals($expected, $fileInfo->getBits());

		$fileInfo->setBits('8bits');
		$this->assertEquals($expected, $fileInfo->getBits());
	}

	public function testSettingMimeType()
	{
		$fileInfo = new ImageInfo();

		$expected = 'image/jpg';

		$fileInfo->setMime($expected);
		$this->assertEquals($expected, $fileInfo->getMime());
	}

} 