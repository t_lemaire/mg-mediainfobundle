<?php

namespace MindGeek\MediaInfoBundle\Tests\Domain\MediaInfo;

use \MindGeek\MediaInfoBundle\Domain\MediaInfo\VideoInfo;

class VideoInfoTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingFormat()
	{
		$videoInfo = new VideoInfo();

		$expected = 'mp4';
		$videoInfo->setFormat($expected);
		$this->assertEquals($expected, $videoInfo->getFormat());
	}

	public function testSettingFormatInfo()
	{
		$videoInfo = new VideoInfo();

		$expected = 'mp4';
		$videoInfo->setFormatInfo($expected);
		$this->assertEquals($expected, $videoInfo->getFormatInfo());
	}

	public function testSettingFormatSettingCABAC()
	{
		$videoInfo = new VideoInfo();

		$expected = 'settings';
		$videoInfo->setFormatSettingsCABAC($expected);
		$this->assertEquals($expected, $videoInfo->getFormatSettingsCABAC());
	}

	public function testSettingFormatSettingReFrames()
	{
		$videoInfo = new VideoInfo();

		$expected = 'settings';
		$videoInfo->setFormatSettingsReFrames($expected);
		$this->assertEquals($expected, $videoInfo->getFormatSettingsReFrames());
	}

	public function testSettingFormatProfile()
	{
		$videoInfo = new VideoInfo();

		$expected = 'profile';
		$videoInfo->setFormatProfile($expected);
		$this->assertEquals($expected, $videoInfo->getFormatProfile());
	}

	public function testSettingCodecId()
	{
		$videoInfo = new VideoInfo();

		$expected = 'codec';
		$videoInfo->setCodecId($expected);
		$this->assertEquals($expected, $videoInfo->getCodecId());
	}

	public function testSettingCodecIdInfo()
	{
		$videoInfo = new VideoInfo();

		$expected = 'codec';
		$videoInfo->setCodecIdInfo($expected);
		$this->assertEquals($expected, $videoInfo->getCodecIdInfo());
	}

	public function testSettingDuration()
	{
		$videoInfo = new VideoInfo();

		$expected = 60;

		$videoInfo->setDuration($expected);
		$this->assertEquals($expected, $videoInfo->getDuration());

		$videoInfo->setDuration('60s');
		$this->assertEquals($expected, $videoInfo->getDuration());
	}

	public function testSettingBitRate()
	{
		$videoInfo = new VideoInfo();

		$expected = 100;

		$videoInfo->setBitRate($expected);
		$this->assertEquals($expected, $videoInfo->getBitRate());

		$videoInfo->setBitRate('100kbps');
		$this->assertEquals($expected, $videoInfo->getBitRate());
	}

	public function testSettingMaximumBitRate()
	{
		$videoInfo = new VideoInfo();

		$expected = 100;

		$videoInfo->setMaximumBitRate($expected);
		$this->assertEquals($expected, $videoInfo->getMaximumBitRate());
	}

	public function testSettingWidth()
	{
		$videoInfo = new VideoInfo();

		$expected = 100;

		$videoInfo->setWidth($expected);
		$this->assertEquals($expected, $videoInfo->getWidth());

		$videoInfo->setWidth('100px');
		$this->assertEquals($expected, $videoInfo->getWidth());
	}

	public function testSettingHeight()
	{
		$videoInfo = new VideoInfo();

		$expected = 100;

		$videoInfo->setHeight($expected);
		$this->assertEquals($expected, $videoInfo->getHeight());

		$videoInfo->setHeight('100px');
		$this->assertEquals($expected, $videoInfo->getHeight());
	}

	public function testSettingDisplayRatio()
	{
		$videoInfo = new VideoInfo();

		$expected = '4:3';
		$videoInfo->setDisplayAspectRatio($expected);
		$this->assertEquals($expected, $videoInfo->getDisplayAspectRatio());
	}

	public function testSettingFrameRateMode()
	{
		$videoInfo = new VideoInfo();

		$expected = 'frame_rate_mode';
		$videoInfo->setFrameRateMode($expected);
		$this->assertEquals($expected, $videoInfo->getFrameRateMode());
	}

	public function testSettingFrameRate()
	{
		$videoInfo = new VideoInfo();

		$expected = '60kbps';
		$videoInfo->setFrameRate($expected);
		$this->assertEquals($expected, $videoInfo->getFrameRate());
	}

	public function testSettingColorSpace()
	{
		$videoInfo = new VideoInfo();

		$expected = 'color+space';
		$videoInfo->setColorSpace($expected);
		$this->assertEquals($expected, $videoInfo->getColorSpace());
	}

	public function testSettingChromaSubSampling()
	{
		$videoInfo = new VideoInfo();

		$expected = 'chroma';
		$videoInfo->setChromaSubSampling($expected);
		$this->assertEquals($expected, $videoInfo->getChromaSubSampling());
	}

	public function testSettingBitDepth()
	{
		$videoInfo = new VideoInfo();

		$expected = 8;
		$videoInfo->setBitDepth($expected);
		$this->assertEquals($expected, $videoInfo->getBitDepth());
	}

	public function testSettingScanType()
	{
		$videoInfo = new VideoInfo();

		$expected = 'cylon_scan';
		$videoInfo->setScanType($expected);
		$this->assertEquals($expected, $videoInfo->getScanType());
	}

	public function testSettingBitsPixelFrame()
	{
		$videoInfo = new VideoInfo();

		$expected = 'bits';
		$videoInfo->setBitsPixelFrame($expected);
		$this->assertEquals($expected, $videoInfo->getBitsPixelFrame());
	}

	public function testSettingStreamSize()
	{
		$videoInfo = new VideoInfo();

		$expected = 10;

		$videoInfo->setStreamSize($expected);
		$this->assertEquals($expected, $videoInfo->getStreamSize());

		$videoInfo->setStreamSize('10b');
		$this->assertEquals($expected, $videoInfo->getStreamSize());
	}

} 