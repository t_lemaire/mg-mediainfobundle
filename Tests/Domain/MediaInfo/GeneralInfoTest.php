<?php

namespace MindGeek\MediaInfoBundle\Tests\Domain\MediaInfo;

use \MindGeek\MediaInfoBundle\Domain\MediaInfo\GeneralInfo;

class GeneralInfoTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingFormat()
	{
		$generalInfo = new GeneralInfo();

		$expected = 'mp4';
		$generalInfo->setFormat($expected);
		$this->assertEquals($expected, $generalInfo->getFormat());
	}

	public function testSettingFormatProfile()
	{
		$generalInfo = new GeneralInfo();

		$expected = 'profile';
		$generalInfo->setFormatProfile($expected);
		$this->assertEquals($expected, $generalInfo->getFormatProfile());
	}

	public function testSettingCodecId()
	{
		$generalInfo = new GeneralInfo();

		$expected = 'codec';
		$generalInfo->setCodecId($expected);
		$this->assertEquals($expected, $generalInfo->getCodecId());
	}

	public function testSettingSize()
	{
		$generalInfo = new GeneralInfo();

		$expected = 100;

		$generalInfo->setFileSize($expected);
		$this->assertEquals($expected, $generalInfo->getFileSize());

		$generalInfo->setFileSize('100Kb');
		$this->assertEquals($expected, $generalInfo->getFileSize());
	}

	public function testSettingDuration()
	{
		$generalInfo = new GeneralInfo();

		$expected = 60;

		$generalInfo->setDuration($expected);
		$this->assertEquals($expected, $generalInfo->getDuration());

		$generalInfo->setDuration('60s');
		$this->assertEquals($expected, $generalInfo->getDuration());
	}

	public function testSettingOverallBitRate()
	{
		$generalInfo = new GeneralInfo();

		$expected = 60;

		$generalInfo->setOverallBitRate($expected);
		$this->assertEquals($expected, $generalInfo->getOverallBitRate());

		$generalInfo->setOverallBitRate('60kbps');
		$this->assertEquals($expected, $generalInfo->getOverallBitRate());
	}

	public function testSettingOverallBitRateMode()
	{
		$generalInfo = new GeneralInfo();

		$expected = 'bit_rate_mode';

		$generalInfo->setOverallBitRateMode($expected);
		$this->assertEquals($expected, $generalInfo->getOverallBitRateMode());
	}

	public function testSettingWritingApplication()
	{
		$generalInfo = new GeneralInfo();

		$expected = 'application';

		$generalInfo->setWritingApplication($expected);
		$this->assertEquals($expected, $generalInfo->getWritingApplication());
	}

}
