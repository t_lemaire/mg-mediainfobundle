<?php

namespace MindGeek\MediaInfoBundle\Tests\Domain\MediaInfo;

use \MindGeek\MediaInfoBundle\Domain\MediaInfo\AudioInfo;

class AudioInfoTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingId()
	{
		$audioInfo = new AudioInfo();

		$expected = 'id';
		$audioInfo->setId($expected);
		$this->assertEquals($expected, $audioInfo->getId());
	}

	public function testSettingFormat()
	{
		$audioInfo = new AudioInfo();

		$expected = 'mp4';
		$audioInfo->setFormat($expected);
		$this->assertEquals($expected, $audioInfo->getFormat());
	}

	public function testSettingFormatInfo()
	{
		$audioInfo = new AudioInfo();

		$expected = 'mp4';
		$audioInfo->setFormatInfo($expected);
		$this->assertEquals($expected, $audioInfo->getFormatInfo());
	}

	public function testSettingFormatProfile()
	{
		$audioInfo = new AudioInfo();

		$expected = 'profile';
		$audioInfo->setFormatProfile($expected);
		$this->assertEquals($expected, $audioInfo->getFormatProfile());
	}

	public function testSettingCodecId()
	{
		$audioInfo = new AudioInfo();

		$expected = 'codec';
		$audioInfo->setCodecId($expected);
		$this->assertEquals($expected, $audioInfo->getCodecId());
	}

	public function testSettingDuration()
	{
		$audioInfo = new AudioInfo();

		$expected = 60;

		$audioInfo->setDuration($expected);
		$this->assertEquals($expected, $audioInfo->getDuration());

		$audioInfo->setDuration('60s');
		$this->assertEquals($expected, $audioInfo->getDuration());
	}

	public function testSettingBitRateMode()
	{
		$audioInfo = new AudioInfo();

		$expected = 'bit_rate_mode';
		$audioInfo->setBitRateMode($expected);
		$this->assertEquals($expected, $audioInfo->getBitRateMode());
	}

	public function testSettingBitRate()
	{
		$audioInfo = new AudioInfo();

		$expected = 100;

		$audioInfo->setBitRate($expected);
		$this->assertEquals($expected, $audioInfo->getBitRate());

		$audioInfo->setBitRate('100kbps');
		$this->assertEquals($expected, $audioInfo->getBitRate());
	}

	public function testSettingMaximumBitRate()
	{
		$audioInfo = new AudioInfo();

		$expected = 100;

		$audioInfo->setMaximumBitRate($expected);
		$this->assertEquals($expected, $audioInfo->getMaximumBitRate());

		$audioInfo->setBitRate('100kbps');
		$this->assertEquals($expected, $audioInfo->getMaximumBitRate());
	}

	public function testSettingChannels()
	{
		$audioInfo = new AudioInfo();

		$expected = 'channels';
		$audioInfo->setChannels($expected);
		$this->assertEquals($expected, $audioInfo->getChannels());
	}

	public function testSettingChannelPositions()
	{
		$audioInfo = new AudioInfo();

		$expected = 'channel_positions';
		$audioInfo->setChannelPositions($expected);
		$this->assertEquals($expected, $audioInfo->getChannelPositions());
	}

	public function testSettingSamplingRate()
	{
		$audioInfo = new AudioInfo();

		$expected = 'sampling_rate';
		$audioInfo->setSamplingRate($expected);
		$this->assertEquals($expected, $audioInfo->getSamplingRate());
	}

	public function testSettingCompressionMode()
	{
		$audioInfo = new AudioInfo();

		$expected = 'ultra';
		$audioInfo->setCompressionMode($expected);
		$this->assertEquals($expected, $audioInfo->getCompressionMode());
	}

	public function testSettingStreamSize()
	{
		$audioInfo = new AudioInfo();

		$expected = 100;
		$audioInfo->setStreamSize($expected);
		$this->assertEquals($expected, $audioInfo->getStreamSize());
	}

} 