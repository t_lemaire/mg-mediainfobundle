<?php

namespace MindGeek\MediaInfoBundle\Tests\Domain;

use \MindGeek\MediaInfoBundle\Domain\FileInfo;

class FileInfoTest extends \PHPUnit_Framework_TestCase
{

	public function testSettingFileName()
	{
		$fileInfo = new FileInfo();

		$expected = 'file.ext';
		$fileInfo->setFileName($expected);
		$this->assertEquals($expected, $fileInfo->getFileName());
	}

	public function testSettingSize()
	{
		$fileInfo = new FileInfo();

		$expected = 100;

		$fileInfo->setFileSize($expected);
		$this->assertEquals($expected, $fileInfo->getFileSize());

		$fileInfo->setFileSize('100Kb');
		$this->assertEquals($expected, $fileInfo->getFileSize());
	}

	public function testSettingExtension()
	{
		$fileInfo = new FileInfo();

		$expected = 'ext';
		$fileInfo->setFileExtension($expected);
		$this->assertEquals($expected, $fileInfo->getFileExtension());
	}

} 