<?php

namespace MindGeek\MediaInfoBundle\Tests\Services;

use \MindGeek\MediaInfoBundle\Services\FileInfo;

class FileInfoTest extends \PHPUnit_Framework_TestCase
{

	/**
	 * @expectedException \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
	 * @expectedExceptionMessage Unable to locate file: /yolo.lol
	 */
	public function testGettingInfoFromAFileThatDoesNotExists()
	{
		$fileInfo = new FileInfo();
		$fileInfo->getInfo('/yolo.lol');
	}

	public function testGettingInfoFromAFileWithoutExtension()
	{
		$fileInfo = new FileInfo();
		$result = $fileInfo->getInfo(__DIR__.'/../Mocks/FileWithNotExtension');

		$this->assertEquals(0, $result->getFileSize());
		$this->assertEquals('FileWithNotExtension', $result->getFileName());
		$this->assertEquals('', $result->getFileExtension());
	}

    public function testGettingInfoFromAFileWithoutExtensionAsArray()
    {
        $fileInfo = new FileInfo();
        $result = $fileInfo->getInfoAsArray(__DIR__.'/../Mocks/FileWithNotExtension');

        $this->assertEquals(0, $result['fileSize']);
        $this->assertEquals('FileWithNotExtension', $result['fileName']);
        $this->assertEquals('', $result['fileExtension']);
    }

	public function testGettingFullInfo()
	{
		$fileInfo = new FileInfo();
		$result = $fileInfo->getInfo(__DIR__.'/../Mocks/file.ext');

		$this->assertEquals(5, $result->getFileSize());
		$this->assertEquals('file', $result->getFileName());
		$this->assertEquals('ext', $result->getFileExtension());
	}

    public function testGettingFullInfoAsArray()
    {
        $fileInfo = new FileInfo();
        $result = $fileInfo->getInfoAsArray(__DIR__.'/../Mocks/file.ext');

        $this->assertEquals(5, $result['fileSize']);
        $this->assertEquals('file', $result['fileName']);
        $this->assertEquals('ext', $result['fileExtension']);
    }

} 