<?php

/**
 * User: Fabrice Baumann - Capitaine - <fabrice.baumann@mindgeek.com>
 * Date: 26/09/16 - 2:55 PM
 */
class HLSDetectorTest extends PHPUnit_Framework_TestCase
{

    /**
     * @expectedException \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @expectedExceptionMessage /Nueva_Lista.lol does not exist or is unreachable
     */
    public function testGettingInfoFromAFileThatDoesNotExists()
    {
        $hlsDetector = new \MindGeek\MediaInfoBundle\Services\Detector\HLSDetector();
        $hlsDetector->detectType('/Nueva_Lista.lol');
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testCheckingHeaderFromPlaylistFile()
    {
        $hlsDetector = new \MindGeek\MediaInfoBundle\Services\Detector\HLSDetector();
        $return = $hlsDetector->detectType(__DIR__ . '/../../Mocks/Nueva_Lista.m3u');

        $this->assertEquals('playlist', $return);
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testCheckingHeaderFromVideoFile()
    {
        $hlsDetector = new \MindGeek\MediaInfoBundle\Services\Detector\HLSDetector();
        $return = $hlsDetector->detectType(__DIR__ . '/../../Mocks/video.mp4');

        $this->assertEquals('unknown', $return);
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testRealVideoFile()
    {
        $hlsDetector = new \MindGeek\MediaInfoBundle\Services\Detector\HLSDetector();
        $res = $hlsDetector->detectType(__DIR__ . '/../../Mocks/480p_370k_12222657.mp4');

        $this->assertEquals('unknown', $res);
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testPngFile()
    {
        $hlsDetector = new \MindGeek\MediaInfoBundle\Services\Detector\HLSDetector();
        $res = $hlsDetector->detectType(__DIR__ . '/../../Mocks/yolo.png');

        $this->assertEquals('unknown', $res);
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testGifFile()
    {
        $hlsDetector = new \MindGeek\MediaInfoBundle\Services\Detector\HLSDetector();
        $res = $hlsDetector->detectType(__DIR__ . '/../../Mocks/giphy.gif');

        $this->assertEquals('unknown', $res);
    }


    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testTxtFile()
    {
        $hlsDetector = new \MindGeek\MediaInfoBundle\Services\Detector\HLSDetector();
        $res = $hlsDetector->detectType(__DIR__ . '/../../Mocks/file.ext');

        $this->assertEquals('unknown', $res);
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testFakeVideoFile()
    {
        $hlsDetector = new \MindGeek\MediaInfoBundle\Services\Detector\HLSDetector();
        $res = $hlsDetector->detectType(__DIR__ . '/../../Mocks/fake_mp4_video_file.mp4');

        $this->assertEquals('playlist', $res);
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testJpgFile()
    {
        $hlsDetector = new \MindGeek\MediaInfoBundle\Services\Detector\HLSDetector();
        $res = $hlsDetector->detectType(__DIR__ . '/../../Mocks/mini.jpg');

        $this->assertEquals('unknown', $res);
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testUnknownFile()
    {
        $hlsDetector = new \MindGeek\MediaInfoBundle\Services\Detector\HLSDetector();
        $res = $hlsDetector->detectType(__DIR__ . '/../../Mocks/FileWithNotExtension');

        $this->assertEquals('unknown', $res);
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testPlaylistFileFakedAsVideo()
    {
        $hlsDetector = new \MindGeek\MediaInfoBundle\Services\Detector\HLSDetector();
        $res = $hlsDetector->detectType(__DIR__ . '/../../Mocks/9143414ffa3adeb7c568d09af69f3d5d.mp4');

        $this->assertEquals('playlist', $res);
    }


}
