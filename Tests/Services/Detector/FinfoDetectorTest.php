<?php

/**
 * User: Fabrice Baumann - Capitaine - <fabrice.baumann@mindgeek.com>
 * Date: 26/09/16 - 4:01 PM
 */
class FinfoDetectorTest extends PHPUnit_Framework_TestCase
{

    /**
     * @expectedException \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @expectedExceptionMessage /tidou.lol does not exist or is unreachable
     */
    public function testFileNotFoundException()
    {
        $finfoDetector = new \MindGeek\MediaInfoBundle\Services\Detector\FinfoDetector();
        $finfoDetector->detectType('/tidou.lol');
    }

    public function testVideoMimeTypeReturn()
    {
        $finfoDetector = new \MindGeek\MediaInfoBundle\Services\Detector\FinfoDetector();
        $mimeType = $finfoDetector->detectType(__DIR__ . '/../../Mocks/video.mp4');

        $this->assertEquals('video/mp4', $mimeType);
    }

    public function testImageMimeTypeReturn()
    {
        $finfoDetector = new \MindGeek\MediaInfoBundle\Services\Detector\FinfoDetector();
        $mimeType = $finfoDetector->detectType(__DIR__ . '/../../Mocks/yolo.png');

        $this->assertEquals('image/png', $mimeType);
    }

    public function testFileMimeTypeReturn()
    {
        $finfoDetector = new \MindGeek\MediaInfoBundle\Services\Detector\FinfoDetector();
        $mimeType = $finfoDetector->detectType(__DIR__ . '/../../Mocks/file.ext');

        $this->assertEquals('text/plain', $mimeType);
    }

}
