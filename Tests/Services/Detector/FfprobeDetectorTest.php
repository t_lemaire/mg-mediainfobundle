<?php

/**
 * User: Fabrice Baumann - Capitaine - <fabrice.baumann@mindgeek.com>
 * Date: 26/09/16 - 4:16 PM
 */
class FfprobeDetectorTest extends PHPUnit_Framework_TestCase
{

    /**
     * @expectedException \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @expectedExceptionMessage /blablka.lolf does not exist or is unreachable
     */
    public function testNonExistingFile()
    {
        $ffprobeDetector = new \MindGeek\MediaInfoBundle\Services\Detector\FfprobeDetector();
        $ffprobeDetector->setRunFfprobeAsNonet(false);
        $ffprobeDetector->detectType('/blablka.lolf');
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testRealVideoFile()
    {
        $ffprobeDetector = new \MindGeek\MediaInfoBundle\Services\Detector\FfprobeDetector();
        $ffprobeDetector->setRunFfprobeAsNonet(false);
        $res = $ffprobeDetector->detectType(__DIR__ . '/../../Mocks/480p_370k_12222657.mp4');

        $this->assertEquals('video', $res);
    }


    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testPngFile()
    {
        $ffprobeDetector = new \MindGeek\MediaInfoBundle\Services\Detector\FfprobeDetector();
        $ffprobeDetector->setRunFfprobeAsNonet(false);
        $res = $ffprobeDetector->detectType(__DIR__ . '/../../Mocks/yolo.png');

        $this->assertEquals('unknown', $res);
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testGifFile()
    {
        $ffprobeDetector = new \MindGeek\MediaInfoBundle\Services\Detector\FfprobeDetector();
        $ffprobeDetector->setRunFfprobeAsNonet(false);
        $res = $ffprobeDetector->detectType(__DIR__ . '/../../Mocks/giphy.gif');

        $this->assertEquals('unknown', $res);
    }


    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testTxtFile()
    {
        $ffprobeDetector = new \MindGeek\MediaInfoBundle\Services\Detector\FfprobeDetector();
        $ffprobeDetector->setRunFfprobeAsNonet(false);
        $res = $ffprobeDetector->detectType(__DIR__ . '/../../Mocks/file.ext');

        $this->assertEquals('unknown', $res);
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testFakeVideoFile()
    {
        $ffprobeDetector = new \MindGeek\MediaInfoBundle\Services\Detector\FfprobeDetector();
        $ffprobeDetector->setRunFfprobeAsNonet(false);
        $res = $ffprobeDetector->detectType(__DIR__ . '/../../Mocks/fake_mp4_video_file.mp4');

        $this->assertEquals('unknown', $res);
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testPlaylistFile()
    {
        $ffprobeDetector = new \MindGeek\MediaInfoBundle\Services\Detector\FfprobeDetector();
        $ffprobeDetector->setRunFfprobeAsNonet(false);
        $res = $ffprobeDetector->detectType(__DIR__ . '/../../Mocks/Nueva_Lista.m3u');

        $this->assertEquals('unknown', $res);
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testJpgFile()
    {
        $ffprobeDetector = new \MindGeek\MediaInfoBundle\Services\Detector\FfprobeDetector();
        $ffprobeDetector->setRunFfprobeAsNonet(false);
        $res = $ffprobeDetector->detectType(__DIR__ . '/../../Mocks/mini.jpg');

        $this->assertEquals('unknown', $res);
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testUnknownFile()
    {
        $ffprobeDetector = new \MindGeek\MediaInfoBundle\Services\Detector\FfprobeDetector();
        $ffprobeDetector->setRunFfprobeAsNonet(false);
        $res = $ffprobeDetector->detectType(__DIR__ . '/../../Mocks/FileWithNotExtension');

        $this->assertEquals('unknown', $res);
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testThreeSecondVideo()
    {
        $ffprobeDetector = new \MindGeek\MediaInfoBundle\Services\Detector\FfprobeDetector();
        $ffprobeDetector->setRunFfprobeAsNonet(false);
        $res = $ffprobeDetector->detectType(__DIR__ . '/../../Mocks/3SecondVideo.mp4');

        $this->assertEquals('unknown', $res);
    }

    /**
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     */
    public function testCustomDurationVideo()
    {
        $ffprobeDetector = new \MindGeek\MediaInfoBundle\Services\Detector\FfprobeDetector();
        $ffprobeDetector->setRunFfprobeAsNonet(false);
        $ffprobeDetector->setMinimumVideoDuration(2);
        $res = $ffprobeDetector->detectType(__DIR__ . '/../../Mocks/3SecondVideo.mp4');

        $this->assertEquals('video', $res);
    }

}
