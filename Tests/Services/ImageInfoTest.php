<?php

namespace MindGeek\MediaInfoBundle\Tests\Services;

use \MindGeek\MediaInfoBundle\Services\ImageInfo;

class ImageInfoTest extends \PHPUnit_Framework_TestCase
{

	/**
	 * @expectedException \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
	 * @expectedExceptionMessage Unable to locate file: /yolo.lol
	 */
	public function testGettingInfoFromAFileThatDoesNotExists()
	{
		$fileInfo = new ImageInfo();
		$fileInfo->getInfo('/yolo.lol');
	}

	public function testGettingImageInfo()
	{
		$imageInfo = new ImageInfo();
		$result = $imageInfo->getInfo(__DIR__.'/../Mocks/yolo.png');

		$this->assertEquals(40426, $result->getFileSize());
		$this->assertEquals('yolo', $result->getFileName());
		$this->assertEquals('png', $result->getFileExtension());
		$this->assertEquals(600, $result->getWidth());
		$this->assertEquals(700, $result->getHeight());
		$this->assertEquals(8, $result->getBits());
		$this->assertEquals('image/png', $result->getMime());
	}

    public function testGettingImageInfoAsArray()
    {
        $imageInfo = new ImageInfo();
        $result = $imageInfo->getInfoAsArray(__DIR__.'/../Mocks/yolo.png');

        $this->assertEquals(40426, $result['fileSize']);
        $this->assertEquals('yolo', $result['fileName']);
        $this->assertEquals('png', $result['fileExtension']);
        $this->assertEquals(600, $result['width']);
        $this->assertEquals(700, $result['height']);
        $this->assertEquals(8, $result['bits']);
        $this->assertEquals('image/png', $result['mime']);
    }



} 