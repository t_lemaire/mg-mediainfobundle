<?php

namespace MindGeek\MediaInfoBundle\Tests\Services;

use \MindGeek\MediaInfoBundle\Services\MediaInfo;

class MediaInfoTest extends \PHPUnit_Framework_TestCase
{

	/**
	 * @expectedException \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
	 * @expectedExceptionMessage Unable to locate file: /yolo.lol
	 */
	public function testGettingInfoAboutAFileThatDoesNotExists()
	{
		$fileInfo = new MediaInfo();
		$fileInfo->getInfo('/yolo.lol');
	}

	/**
	 * @expectedException \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
	 * @expectedExceptionMessage Unexpected data structure, unable to get xml->File->track
	 */
	public function testGettingInfoAboutAFileThatReturnsANonXMLFormatInfo()
	{
		$fileInfo = new MediaInfoMock();
		$fileInfo->getInfo(__DIR__.'/../Mocks/video.mp4');
	}

	public function testGettingMediaInfo()
	{
		$imageInfo = new MediaInfo();
		$result = $imageInfo->getInfo(__DIR__.'/../Mocks/video.mp4');

		$generalInfo = $result->getGeneralInfo();
		$videoInfo = $result->getVideoInfo();
		$audio = $result->getAudioInfo();

		// File info assertions
		$this->assertEquals(1606388, $result->getFileSize());
		$this->assertEquals('video', $result->getFileName());
		$this->assertEquals('mp4', $result->getFileExtension());

		// General info assertions
		$this->assertEquals('MPEG-4', $generalInfo->getFormat());
		$this->assertEquals('Base Media', $generalInfo->getFormatProfile());
		$this->assertEquals('isom', $generalInfo->getCodecId());
		$this->assertEquals(1604321, $generalInfo->getFileSize());
		$this->assertEquals(10, $generalInfo->getDuration());
		$this->assertEquals(1284000, $generalInfo->getOverallBitRate());
		$this->assertEquals('Lavf55.19.104', $generalInfo->getWritingApplication());

		// Video info assertions
		$this->assertEquals('AVC', $videoInfo->getFormat());
		$this->assertEquals('Advanced Video Codec', $videoInfo->getFormatInfo());
		$this->assertEquals('High@L3.1', $videoInfo->getFormatProfile());
		$this->assertEquals('Yes', $videoInfo->getFormatSettingsCABAC());
		$this->assertEquals('4 frames', $videoInfo->getFormatSettingsReFrames());
		$this->assertEquals('avc1', $videoInfo->getCodecId());
		$this->assertEquals('Advanced Video Coding', $videoInfo->getCodecIdInfo());
		$this->assertEquals(9, $videoInfo->getDuration());
		$this->assertEquals(1227000, $videoInfo->getBitRate());
		$this->assertEquals(0, $videoInfo->getMaximumBitRate());
		$this->assertEquals(1280, $videoInfo->getWidth());
		$this->assertEquals(720, $videoInfo->getHeight());
		$this->assertEquals('16:9', $videoInfo->getDisplayAspectRatio());
		$this->assertEquals('Constant', $videoInfo->getFrameRateMode());
		$this->assertEquals('23.976 fps', $videoInfo->getFrameRate());
		$this->assertEquals('YUV', $videoInfo->getColorSpace());
		$this->assertEquals('4:2:0', $videoInfo->getChromaSubSampling());
		$this->assertEquals('8 bits', $videoInfo->getBitDepth());
		$this->assertEquals('Progressive', $videoInfo->getScanType());
		$this->assertEquals('0.056', $videoInfo->getBitsPixelFrame());
		$this->assertEquals(1436549, $videoInfo->getStreamSize());

		// Audio info assertions
		$this->assertEquals('2', $audio->getId());
		$this->assertEquals('AAC', $audio->getFormat());
		$this->assertEquals('Advanced Audio Codec', $audio->getFormatInfo());
		$this->assertEquals('LC', $audio->getFormatProfile());
		$this->assertEquals('40', $audio->getCodecId());
		$this->assertEquals(10, $audio->getDuration());
		$this->assertEquals('Constant', $audio->getBitRateMode());
		$this->assertEquals(128000, $audio->getBitRate());
		$this->assertEquals(0, $audio->getMaximumBitRate());
		$this->assertEquals('2 channels', $audio->getChannels());
		$this->assertEquals('Front: L R', $audio->getChannelPositions());
		$this->assertEquals('44.1 KHz', $audio->getSamplingRate());
		$this->assertEquals('Lossy', $audio->getCompressionMode());
		$this->assertEquals(159744, $audio->getStreamSize());
	}

    public function testGettingMediaInfoAsArray()
    {
        $imageInfo = new MediaInfo();
        $result = $imageInfo->getInfoAsArray(__DIR__.'/../Mocks/video.mp4');

        $generalInfo = $result['generalInfo'];
        $videoInfo = $result['videoInfo'];
        $audio = $result['audioInfo'];

        // File info assertions
        $this->assertEquals(1606388, $result['fileSize']);
        $this->assertEquals('video', $result['fileName']);
        $this->assertEquals('mp4', $result['fileExtension']);

        // General info assertions
        $this->assertEquals('MPEG-4', $generalInfo['format']);
        $this->assertEquals('Base Media', $generalInfo['formatProfile']);
        $this->assertEquals('isom', $generalInfo['codecId']);
        $this->assertEquals(1604321, $generalInfo['fileSize']);
        $this->assertEquals(10, $generalInfo['duration']);
        $this->assertEquals(1284000, $generalInfo['overallBitRate']);
        $this->assertEquals('Lavf55.19.104', $generalInfo['writingApplication']);

        // Video info assertions
        $this->assertEquals('AVC', $videoInfo['format']);
        $this->assertEquals('Advanced Video Codec', $videoInfo['formatInfo']);
        $this->assertEquals('High@L3.1', $videoInfo['formatProfile']);
        $this->assertEquals('Yes', $videoInfo['formatSettingsCABAC']);
        $this->assertEquals('4 frames', $videoInfo['formatSettingsReFrames']);
        $this->assertEquals('avc1', $videoInfo['codecId']);
        $this->assertEquals('Advanced Video Coding', $videoInfo['codecIdInfo']);
        $this->assertEquals(9, $videoInfo['duration']);
        $this->assertEquals(1227000, $videoInfo['bitRate']);
        $this->assertEquals(0, $videoInfo['maximumBitRate']);
        $this->assertEquals(1280, $videoInfo['width']);
        $this->assertEquals(720, $videoInfo['height']);
        $this->assertEquals('16:9', $videoInfo['displayAspectRatio']);
        $this->assertEquals('Constant', $videoInfo['frameRateMode']);
        $this->assertEquals('23.976 fps', $videoInfo['frameRate']);
        $this->assertEquals('YUV', $videoInfo['colorSpace']);
        $this->assertEquals('4:2:0', $videoInfo['chromaSubSampling']);
        $this->assertEquals('8 bits', $videoInfo['bitDepth']);
        $this->assertEquals('Progressive', $videoInfo['scanType']);
        $this->assertEquals('0.056', $videoInfo['bitsPixelFrame']);
        $this->assertEquals(1436549, $videoInfo['streamSize']);

        // Audio info assertions
        $this->assertEquals('2', $audio['id']);
        $this->assertEquals('AAC', $audio['format']);
        $this->assertEquals('Advanced Audio Codec', $audio['formatInfo']);
        $this->assertEquals('LC', $audio['formatProfile']);
        $this->assertEquals('40', $audio['codecId']);
        $this->assertEquals(10, $audio['duration']);
        $this->assertEquals('Constant', $audio['bitRateMode']);
        $this->assertEquals(128000, $audio['bitRate']);
        $this->assertEquals(0, $audio['maximumBitRate']);
        $this->assertEquals('2 channels', $audio['channels']);
        $this->assertEquals('Front: L R', $audio['channelPositions']);
        $this->assertEquals('44.1 KHz', $audio['samplingRate']);
        $this->assertEquals('Lossy', $audio['compressionMode']);
        $this->assertEquals(159744, $audio['streamSize']);
    }

	public function testGettingFileSizeValueForAnInvalidValue()
	{
		$imageInfo = new MediaInfo();

		$this->assertNull($imageInfo->getFileSize(null));
		$this->assertNull($imageInfo->getFileSize(''));
		$this->assertNull($imageInfo->getFileSize('  '));
	}

	public function testGettingFileSizeValueForASetOfValidValues()
	{
		$imageInfo = new MediaInfo();

		$expected = 100;

		$this->assertEquals($expected, $imageInfo->getFileSize(100));
		$this->assertEquals($expected, $imageInfo->getFileSize('100'));
		$this->assertEquals($expected, $imageInfo->getFileSize('100 '));
		$this->assertEquals($expected, $imageInfo->getFileSize('100 bytes'));
		$this->assertEquals($expected*1024, $imageInfo->getFileSize('100 kib'));
		$this->assertEquals($expected*1024*1024, $imageInfo->getFileSize('100 mib'));
		$this->assertEquals($expected*1024*1024*1024, $imageInfo->getFileSize('100 gib'));
		$this->assertEquals($expected*1024*1024*1024*1024, $imageInfo->getFileSize('100 tib'));
	}

	public function testGettingSecondsValueForAnInvalidValue()
	{
		$imageInfo = new MediaInfo();

		$this->assertNull($imageInfo->getSeconds(null));
		$this->assertNull($imageInfo->getSeconds(''));
		$this->assertNull($imageInfo->getSeconds('  '));
	}

	public function testGettingSecondsValueForASetOfValidValues()
	{
		$imageInfo = new MediaInfo();

		$this->assertEquals(0, $imageInfo->getSeconds(100));
		$this->assertEquals(0, $imageInfo->getSeconds('100'));
		$this->assertEquals(0, $imageInfo->getSeconds('100min'));
		$this->assertEquals(0, $imageInfo->getSeconds('100ms'));
		$this->assertEquals(100, $imageInfo->getSeconds('100s'));
		$this->assertEquals(3600, $imageInfo->getSeconds('1h'));
		$this->assertEquals(60, $imageInfo->getSeconds('1mn'));
	}

	public function testGettingKbpsValueForAnInvalidValue()
	{
		$imageInfo = new MediaInfo();

		$this->assertNull($imageInfo->getBitsPerSecond(null));
		$this->assertNull($imageInfo->getBitsPerSecond(''));
		$this->assertNull($imageInfo->getBitsPerSecond('  '));
	}

	public function testGettingKbpsValueForASetOfValidValues()
	{
		$imageInfo = new MediaInfo();

		$this->assertEquals(100, $imageInfo->getBitsPerSecond(100));
		$this->assertEquals(100, $imageInfo->getBitsPerSecond('100'));
		$this->assertEquals(100, $imageInfo->getBitsPerSecond('100bps'));
		$this->assertEquals(100*1000, $imageInfo->getBitsPerSecond('100kbps'));
		$this->assertEquals(100*1000*1000, $imageInfo->getBitsPerSecond('100mbps'));
		$this->assertEquals(100*1000*1000*1000, $imageInfo->getBitsPerSecond('100gbps'));
		$this->assertEquals(100*1000*1000*1000*1000, $imageInfo->getBitsPerSecond('100tbps'));

	}

}

class MediaInfoMock extends MediaInfo
{
	/**
	 * @param string $filePath
	 *
	 * @return array|string
	 */
	protected  function obtainMediaInfo($filePath)
	{
		return array('<xml></xml>');
	}

}
