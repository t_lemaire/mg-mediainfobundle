<?php

namespace MindGeek\MediaInfoBundle\Services;

/**
 * Gets the media info from the console mediainfo command
 *
 * mediainfo command must be installed and enabled. (http://mediaarea.net/en/MediaInfo)
 */
class ImageInfo extends AbstractInfo
{

	/**
	 * @param string $filePath
	 *
	 * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
	 * @return \MindGeek\MediaInfoBundle\Domain\ImageInfo
	 */
	public function getInfo($filePath)
	{
		$fileInfoService = new FileInfo();
		$fileInfo = $fileInfoService->getInfo($filePath);

		$info = getimagesize($filePath, $details);

		$imageInfo = new \MindGeek\MediaInfoBundle\Domain\ImageInfo();

		if (isset($info[0])) {
			$imageInfo->setWidth($info[0]);
		}

		if (isset($info[1])) {
			$imageInfo->setHeight($info[1]);
		}

		if (isset($info['bits'])) {
			$imageInfo->setBits($info['bits']);
		}

		if (isset($info['mime'])) {
			$imageInfo->setMime($info['mime']);
		}

		$imageInfo
			->setFileExtension($fileInfo->getFileExtension())
			->setFileName($fileInfo->getFileName())
			->setFileSize($fileInfo->getFileSize());

		return $imageInfo;
	}

    /**
     * @param string $filePath
     *
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @return array
     */
    public function getInfoAsArray($filePath)
    {
        $fileInfoService = new FileInfo();
        $fileInfo = $fileInfoService->getInfoAsArray($filePath);

        $info = getimagesize($filePath, $details);

        if (isset($info[0])) {
            $fileInfo['width'] = $info[0];
        }

        if (isset($info[1])) {
            $fileInfo['height'] = $info[1];
        }

        if (isset($info['bits'])) {
            $fileInfo['bits'] = $info['bits'];
        }

        if (isset($info['mime'])) {
            $fileInfo['mime'] = $info['mime'];
        }

        return $fileInfo;
    }

}