<?php

namespace MindGeek\MediaInfoBundle\Services;

use \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException;

/**
 * Gets the media info from the console mediainfo command
 *
 * mediainfo command must be installed and enabled. (http://mediaarea.net/en/MediaInfo)
 */
class FileInfo extends AbstractInfo
{

	/**
	 * @param string $filePath
	 *
	 * @return \MindGeek\MediaInfoBundle\Domain\FileInfo
	 * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
	 */
	public function getInfo($filePath)
	{
		if (!is_file($filePath)) {
			throw new FileNotFoundException('Unable to locate file: ' . $filePath);
		}

		$pathInfo = pathinfo($filePath);

		$fileInfo = new \MindGeek\MediaInfoBundle\Domain\FileInfo();

		if (isset($pathInfo['extension'])) {
			$fileInfo->setFileExtension($pathInfo['extension']);
		}

		$fileInfo->setFileName($pathInfo['filename']);
		$fileInfo->setFileSize(filesize($filePath));

		return $fileInfo;
	}

    /**
     * @param string $filePath
     *
     * @return array
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     */
    public function getInfoAsArray($filePath)
    {
        if (!is_file($filePath)) {
            throw new FileNotFoundException('Unable to locate file: ' . $filePath);
        }

        $pathInfo = pathinfo($filePath);

        $fileInfo = array();

        $fileInfo['fileExtension'] = '';
        if (isset($pathInfo['extension'])) {
            $fileInfo['fileExtension'] = $pathInfo['extension'];
        }

        $fileInfo['fileName'] = $pathInfo['filename'];
        $fileInfo['fileSize'] = filesize($filePath);

        return $fileInfo;
    }

}