<?php
/**
 * User: Fabrice Baumann - Capitaine - <fabrice.baumann@mindgeek.com>
 * Date: 23/09/16 - 3:19 PM
 */

namespace MindGeek\MediaInfoBundle\Services\Detector;


use MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException;

/**
 * Class HLSDetector
 * @package MindGeek\MediaInfoBundle\Services\Detector
 */
class HLSDetector implements DetectorInterface
{

    const BINARY_NAME = 'checkForHLSHeader.sh';
    const PLAYLIST_HEADER = "#EXTM3U";

    /**
     * @param string $filePath
     * @return string
     */
    public function detectType($filePath)
    {
        $detectionResult = $this->hasHLSHeader($filePath);
        if ($detectionResult === true) {
            return 'playlist';
        }

        return 'unknown';
    }


    /**
     * This detector will check that the firl line of the file does not contain #EXTM3U
     *
     * @param $filePath
     * @return bool
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     */
    public function hasHLSHeader($filePath)
    {
        // The best way I found to do the detection for playlist file was to use a small golang binary
        // I have included that binary in the project, and I am executing it through PHP

        if (!is_file($filePath)) {
            throw new FileNotFoundException($filePath . ' does not exist or is unreachable');
        }

        $exitCode = $this->executeDetection($filePath);
        if ($exitCode === 1) {
            return true;
        }

        return false;

    }

    /**
     * @param $filePath
     * @return int
     */
    private function executeDetection($filePath) {
        $escapedPath = htmlspecialchars($filePath);
        $handle = fopen($escapedPath,"r");
        $input = fread($handle,filesize($escapedPath));
        if($this->isPlaylist($input)) {
            return 1;
        }
        elseif(!$this->isValidXml($input)) {
            return 0;
        }
        else {
            return 1;
        }
    }


    /**
     * Method will determine if the file is a valid xml file.
     *
     * @param $input
     * @return bool
     */
    private function isValidXml($input) {
        $isXml = simplexml_load_string($input,"SimpleXMLElement",LIBXML_NOERROR);
        if($isXml === false) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Method will determine if the file is a playlist.
     *
     * @param $input
     * @return bool
     */
    private function isPlaylist($input) {
        $header = substr($input,0,strlen(self::PLAYLIST_HEADER));
        if($header === self::PLAYLIST_HEADER) {
            return true;
        }
        else
            return false;
    }

    /**
     * @param string $filePath
     * @return string
     */
    /*protected function executeExternalDetection($filePath)
    {
        $binaryPath = __DIR__ . '/../../bin/' . self::BINARY_NAME;
        $output = null;
        $exitCode = null;
        exec($binaryPath . ' -q ' . escapeshellarg($filePath) . ' 2>&1', $output, $exitCode);

        return (int)$exitCode;
    }*/
}