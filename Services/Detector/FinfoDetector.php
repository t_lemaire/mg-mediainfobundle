<?php
/**
 * User: Fabrice Baumann - Capitaine - <fabrice.baumann@mindgeek.com>
 * Date: 23/09/16 - 2:48 PM
 */


namespace MindGeek\MediaInfoBundle\Services\Detector;

use MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException;


/**
 * Class FinfoDetector
 * @package MindGeek\MediaInfoBundle\Services\Detector
 */
class FinfoDetector implements DetectorInterface
{

	/**
	 * @var string
	 */
	private $finfoDatabasePath;

	/**
	 * @param string $finfoDatabasePath
	 * @return FinfoDetector
	 */
	public function setFinfoDatabasePath($finfoDatabasePath)
	{
		$this->finfoDatabasePath = $finfoDatabasePath;

		return $this;
	}


	/**
	 * @param string $filePath
	 * @return mixed
	 * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
	 */
	public function detectType($filePath)
	{
		if(!is_file($filePath)) {
			throw new FileNotFoundException($filePath . ' does not exist or is unreachable');
		}

		$fInfo = finfo_open(FILEINFO_MIME_TYPE, $this->finfoDatabasePath);
		$mimeType = finfo_file($fInfo, $filePath);
		finfo_close($fInfo);

		return $mimeType;
	}
}