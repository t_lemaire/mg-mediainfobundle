<?php
/**
 * User: Fabrice Baumann - Capitaine - <fabrice.baumann@mindgeek.com>
 * Date: 23/09/16 - 3:09 PM
 */

namespace MindGeek\MediaInfoBundle\Services\Detector;


/**
 * Interface DetectorInterface
 * @package MindGeek\MediaInfoBundle\Services\Detector
 */
interface DetectorInterface
{
	/**
	 * @param string $filePath
	 * @return string
	 */
	public function detectType($filePath);
}