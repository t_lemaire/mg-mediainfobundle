<?php
/**
 * User: Fabrice Baumann - Capitaine - <fabrice.baumann@mindgeek.com>
 * Date: 23/09/16 - 3:08 PM
 */

namespace MindGeek\MediaInfoBundle\Services\Detector;


use MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException;
use MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException;

/**
 * Class FfprobeDetector
 * @package MindGeek\MediaInfoBundle\Services\Detector
 */
class FfprobeDetector implements DetectorInterface
{

    /**
     * @var bool
     */
    private $countFrames = false;
    /**
     * @var bool
     */
    private $showStreams = true;
    /**
     * @var string
     */
    private $selectStreams = 'v';
    /**
     * @var bool
     */
    private $showFormat = true;
    /**
     * @var string
     */
    private $printFormat = 'json';
    /**
     * @var int
     */
    private $commandTimeout = 20;

    /**
     * @var int
     */
    private $minimumVideoDuration = 4;

    /**
     * @var int default minimum duration to 4 seconds
     */
    private $minimumFramePerSecond = null;

    /**
     * @var bool
     */
    private $runFfprobeAsNonet = true;

    /**
     * @param boolean $runFfprobeAsNonet
     * @return FfprobeDetector
     */
    public function setRunFfprobeAsNonet($runFfprobeAsNonet)
    {
        $this->runFfprobeAsNonet = $runFfprobeAsNonet;

        return $this;
    }

    /**
     * @param int|null $minimumVideoDuration
     * @return FfprobeDetector
     */
    public function setMinimumVideoDuration($minimumVideoDuration)
    {
        $this->minimumVideoDuration = $minimumVideoDuration;

        return $this;
    }

    /**
     * @param null|int $minimumFramePerSecond
     * @return FfprobeDetector
     */
    public function setMinimumFramePerSecond($minimumFramePerSecond)
    {
        $this->minimumFramePerSecond = $minimumFramePerSecond;

        return $this;
    }

    /**
     * @param int $commandTimeout
     * @return FfprobeDetector
     */
    public function setCommandTimeout($commandTimeout)
    {
        $this->commandTimeout = $commandTimeout;

        return $this;
    }

    /**
     * @param string $selectStreams
     * @return FfprobeDetector
     */
    public function setSelectStreams($selectStreams)
    {
        $selectStreams = substr($selectStreams, 0, 1);
        if ($selectStreams !== 'v' && $selectStreams !== 'a') {
            $selectStreams = null;
        }
        $this->selectStreams = $selectStreams;

        return $this;
    }

    /**
     * @param boolean $countFrames
     * @return FfprobeDetector
     */
    public function setCountFrames($countFrames)
    {
        $this->countFrames = (bool)$countFrames;

        return $this;
    }

    /**
     * @param boolean $showStreams
     * @return FfprobeDetector
     */
    public function setShowStreams($showStreams)
    {
        $this->showStreams = (bool)$showStreams;

        return $this;
    }

    /**
     * @param boolean $showFormat
     * @return FfprobeDetector
     */
    public function setShowFormat($showFormat)
    {
        $this->showFormat = (bool)$showFormat;

        return $this;
    }

    /**
     * @param string $printFormat
     * @return FfprobeDetector
     */
    public function setPrintFormat($printFormat)
    {
        $validFormats = ['json', 'xml', 'default', 'ini', 'csv', 'flat'];
        if (!in_array($printFormat, $validFormats)) {
            $printFormat = 'default';
        }
        $this->printFormat = $printFormat;

        return $this;
    }

    /**
     * @param string $filePath
     * @return string
     */
    public function detectType($filePath)
    {
        if (!is_file($filePath)) {
            throw new FileNotFoundException($filePath . ' does not exist or is unreachable');
        }

        $jsonData = $this->obtainMediaInformation($filePath);
        if (empty($jsonData)) {
            return 'unknown';
        }

        $parsedJsonData = json_decode($jsonData, true);
        if (!$parsedJsonData) {
            throw new UnexpectedDataStructureException('Unexpected data structure, unable to parse json data');
        }

        $res = $this->detectVideoType($parsedJsonData);
        if ($res === true) {
            return 'video';
        }

        return 'unknown';
    }

    /**
     * @param string $filePath
     * @return bool|string
     */
    protected function obtainMediaInformation($filePath)
    {
        $output = [];
        $command = $this->createCommandFromParameters($filePath);
        exec($command, $output);

        // If nothing is detected by ffprobe, the output array will just contain { }
        // This way we don't even have to parse that as we know nothing has been detected
        if (!isset($output[3])) {
            return false;
        }

        return implode("\n", $output);
    }

    /**
     * @param string $filePath
     * @return string
     */
    protected function createCommandFromParameters($filePath)
    {
        $cmd = '';
        if ($this->runFfprobeAsNonet === true) {
            $cmd .= 'sg nonet \'';
        }
        $cmd .= 'timeout ' . $this->commandTimeout . ' ffprobe -i ' . escapeshellarg($filePath);
        $cmd .= ' -hide_banner -v quiet -show_entries format=duration -show_entries format=format_name';
        $cmd .= ' -print_format ' . $this->printFormat;

        if ($this->showStreams) {
            $cmd .= ' -show_streams';
        }

        if ($this->countFrames) {
            $cmd .= ' -count_frames';
        }

        if (!empty($this->selectStreams)) {
            $cmd .= ' -select_streams ' . $this->selectStreams;
        }

        if ($this->showFormat) {
            $cmd .= ' -show_format';
        }

        if ($this->runFfprobeAsNonet) {
            $cmd .= '\'';
        }

        return $cmd;
    }

    /**
     * @param $parsedJsonData
     * @return bool
     */
    private function detectVideoType($parsedJsonData)
    {
        /*
         * So .... ffmpeg detects images as having a video stream so we need to add more detections
         *      than just checking if there is a video stream.
         * List of checks:
         *      - if there is a video stream
         *      - if there is a duration
         *      - match the codec against a list of possible values in the mapping class
         *      - check the avg_frame_rate value returned by ffmpeg
         *      - check the format_name value
         */

        // If there is no video stream
        $videoStream = $this->checkForVideoStream($parsedJsonData);
        if ($videoStream === false || !is_array($videoStream)) {
            return false;
        }

        // verify if the format_name contains "image" or something similar
        $result = $this->matchFormatName($parsedJsonData);
        if ($result === false) {
            return false;
        }

        if ($this->minimumVideoDuration !== null) {
            // if there is no video duration
            // Return either false for unknown, or video duration
            $videoDuration = $this->checkForVideoDuration($parsedJsonData);
            if ($videoDuration === false) {
                return false;
            }
        }

        if ($this->minimumFramePerSecond !== null) {
            // number of frames. If lower than XXFPs will get rejected
            $result = $this->checkForNumberReadFramePerSecond($videoStream);
            if ($result === false) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $parsedJsonData
     * @return bool
     */
    private function checkForVideoStream(array $parsedJsonData)
    {
        if (!isset($parsedJsonData['streams']) || empty($parsedJsonData['streams'])) {
            return false;
        }

        $streams = $parsedJsonData['streams'];
        foreach ($streams as $stream) {
            if (isset($stream['codec_type'])) {
                if (stripos($stream['codec_type'], 'video') !== false) {
                    return $stream;
                }
            }
        }

        return false;
    }

    /**
     * @param array $parsedJsonData
     * @return bool|int
     */
    private function checkForVideoDuration(array $parsedJsonData)
    {
        if (!isset($parsedJsonData['format'], $parsedJsonData['format']['duration'])) {
            return false;
        }

        $duration = 0;
        if (!empty($parsedJsonData['format']['duration'])) {
            $duration = $parsedJsonData['format']['duration'];
        }

        if ($duration > $this->minimumVideoDuration) {
            return $duration;
        }

        return false;
    }

    /**
     * @param $videoStream
     * @return bool
     */
    private function checkForNumberReadFramePerSecond($videoStream)
    {
        if (empty($videoStream['avg_frame_rate'])) {
            return false;
        }

        $averageFrameRate = $videoStream['avg_frame_rate'];
        $averageFrameRate = explode('/', $averageFrameRate);
        if (empty($averageFrameRate[0]) || empty($averageFrameRate[1])) {
            return false;
        }

        $framePerSecond = $averageFrameRate[0] / $averageFrameRate[1];
        if ($framePerSecond < $this->minimumFramePerSecond) {
            return false;
        }

        return true;
    }

    /**
     * @param array $parsedJsonData
     * @return bool
     */
    private function matchFormatName(array $parsedJsonData)
    {
        if (isset($parsedJsonData['format'], $parsedJsonData['format']['format_name'])) {
            $videoPattern = ['video'];
            $nonVideoPattern = [
                'image',
                'gif',
                'png',
                'jpg',
                'jpeg',
                'bmp',
                'svg',
                'ico',
                'webp',
                'tiff',
                'vnd.([a-z]+)',
                'hls',
                'http',
            ];

            $formatName = $parsedJsonData['format']['format_name'];
            if (preg_match('/' . implode('|', $videoPattern) . '/', $formatName) === 1) {
                return true;
            }

            if (preg_match('/' . implode('|', $nonVideoPattern) . '/', $formatName) === 1) {
                return false;
            }
        }

        return true;
    }
}