<?php

namespace MindGeek\MediaInfoBundle\Services;

abstract class AbstractInfo
{

	/**
	 * @param $filePath
	 *
	 * @return
	 */
	abstract public function getInfo($filePath);

    /**
     * @param $filePath
     * @return array
     */
    abstract public function getInfoAsArray($filePath);

}