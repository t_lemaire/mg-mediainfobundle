<?php

namespace MindGeek\MediaInfoBundle\Services;

use \MindGeek\MediaInfoBundle\Domain\MediaInfo\AudioInfo;
use \MindGeek\MediaInfoBundle\Domain\MediaInfo\GeneralInfo;
use \MindGeek\MediaInfoBundle\Domain\MediaInfo\VideoInfo;
use MindGeek\MediaInfoBundle\Exceptions\TimeoutException;
use \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException;

/**
 * Gets the media info from the console mediainfo command
 *
 * mediainfo command must be installed and enabled. (http://mediaarea.net/en/MediaInfo)
 */
class MediaInfo extends AbstractInfo
{

	/**
	 * Scans and returns the media info of the provided $filePath
	 * Throws an Exception if the returned XML data could not be parsed.
	 *
	 * @param string $filePath
	 *
	 * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
	 * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @throws TimeoutException
	 * @return \MindGeek\MediaInfoBundle\Domain\MediaInfo
	 */
	public function getInfo($filePath)
	{
		$fileInfoService = new FileInfo();
		$fileInfo = $fileInfoService->getInfo($filePath);

		$data = new \SimpleXMLElement(implode("\n", $this->obtainMediaInfo($filePath)));

		if (!property_exists($data, 'File') && !property_exists($data->File, 'track')) {
			throw new UnexpectedDataStructureException('Unexpected data structure, unable to get xml->File->track');
		}
        if(!isset($data->File->track[0]->Duration)) {
            throw new UnexpectedDataStructureException('Corrupted video found as data structure is missing duration.');
        }
		$generalInfo = new GeneralInfo();
		$videoInfo   = new VideoInfo();
		$audioInfo   = new AudioInfo();

		foreach ($data->File->track as $track) {

			$type = (string)$track['type'];

			if ($type == 'General') {
				$generalInfo
					->setFormat((string)$track->Format)
					->setFormatProfile((string)$track->Format_profile)
					->setCodecId((string)$track->Codec_ID)
					->setFileSize((int) $this->getFileSize($track->File_size))
					->setDuration($this->getSeconds($track->Duration))
					->setOverallBitRateMode((string)$track->Overall_bit_rate_mode)
					->setOverallBitRate($this->getBitsPerSecond($track->Overall_bit_rate))
					->setWritingApplication((string)$track->Writing_application);
			}

			if ($type == 'Video') {
				$videoInfo
					->setFormat((string)$track->Format)
					->setFormatInfo((string)$track->Format_Info)
					->setFormatProfile((string)$track->Format_profile)
					->setFormatSettingsCABAC((string)$track->Format_settings__CABAC)
					->setFormatSettingsReFrames((string)$track->Format_settings__ReFrames)
					->setCodecId((string)$track->Codec_ID)
					->setCodecIdInfo((string)$track->Codec_ID_Info)
					->setDuration($this->getSeconds($track->Duration))
					->setBitRate($this->getBitsPerSecond($track->Bit_rate))
					->setMaximumBitRate($this->getBitsPerSecond($track->Maximum_bit_rate))
					->setWidth((int)preg_replace('/\s/', '', $track->Width))
					->setHeight((int)preg_replace('/\s/', '', $track->Height))
					->setDisplayAspectRatio((string)$track->Display_aspect_ratio)
                    ->setOriginalDisplayAspectRatio((string)$track->Original_display_aspect_ratio)
					->setFrameRateMode((string)$track->Frame_rate_mode)
					->setFrameRate((string)$track->Frame_rate)
					->setColorSpace((string)$track->Color_space)
					->setChromaSubSampling((string)$track->Chroma_subsampling)
					->setBitDepth((string)$track->Bit_depth)
					->setScanType((string)$track->Scan_type)
					->setBitsPixelFrame((string)$track->Bits__Pixel_Frame_)
					->setStreamSize($this->getFileSize($track->Stream_size));
				if (isset($track->Original_width)) {
					$videoInfo->setWidth((int)preg_replace('/\s/', '', $track->Original_width));
				}
				if (isset($track->Original_height)) {
					$videoInfo->setHeight((int)preg_replace('/\s/', '', $track->Original_height));
				}
			}

			if ($type == 'Audio') {
				$audioInfo
					->setId((string)$track->ID)
					->setFormat((string)$track->Format)
					->setFormatInfo((string)$track->Format_Info)
					->setFormatProfile((string)$track->Format_profile)
					->setCodecId((string)$track->Codec_ID)
					->setDuration($this->getSeconds($track->Duration))
					->setBitRateMode((string)$track->Bit_rate_mode)
					->setBitRate($this->getBitsPerSecond($track->Bit_rate))
					->setMaximumBitRate($this->getBitsPerSecond($track->Maximum_bit_rate))
					->setChannels((string)$track->Channel_s_)
					->setChannelPositions((string)$track->Channel_positions)
					->setSamplingRate((string)$track->Sampling_rate)
					->setCompressionMode((string)$track->Compression_mode)
					->setStreamSize($this->getFileSize($track->Stream_size))
				;
			}
		}

		$mediaInfo = new \MindGeek\MediaInfoBundle\Domain\MediaInfo();
		$mediaInfo
			->setGeneralInfo($generalInfo)
			->setVideoInfo($videoInfo)
			->setAudioInfo($audioInfo)
			->setFileExtension($fileInfo->getFileExtension())
			->setFileName($fileInfo->getFileName())
			->setFileSize($fileInfo->getFileSize());

		return $mediaInfo;
	}

    /**
     * Scans and returns the media info of the provided $filePath
     * Throws an Exception if the returned XML data could not be parsed.
     *
     * @param string $filePath
     *
     * @throws \MindGeek\MediaInfoBundle\Exceptions\UnexpectedDataStructureException
     * @throws \MindGeek\MediaInfoBundle\Exceptions\FileNotFoundException
     * @return array
     */
    public function getInfoAsArray($filePath)
    {
        $fileInfoService = new FileInfo();
        $fileInfo = $fileInfoService->getInfoAsArray($filePath);

        $data = new \SimpleXMLElement(implode("\n", $this->obtainMediaInfo($filePath)));

        if (!property_exists($data, 'File') && !property_exists($data->File, 'track')) {
            throw new UnexpectedDataStructureException('Unexpected data structure, unable to get xml->File->track');
        }

        $generalInfo = array();
        $videoInfo   = array();
        $audioInfo   = array();

        foreach ($data->File->track as $track) {

            $type = (string)$track['type'];

            if ($type == 'General') {
                $generalInfo['format'] = (string)$track->Format;
                $generalInfo['formatProfile'] = (string)$track->Format_profile;
                $generalInfo['codecId'] = (string)$track->Codec_ID;
                $generalInfo['fileSize'] = (int) $this->getFileSize($track->File_size);
                $generalInfo['duration'] = $this->getSeconds($track->Duration);
                $generalInfo['overallBitRateMode'] = (string)$track->Overall_bit_rate_mode;
                $generalInfo['overallBitRate'] = $this->getBitsPerSecond($track->Overall_bit_rate);
                $generalInfo['writingApplication'] = (string)$track->Writing_application;
            }

            if ($type == 'Video') {
                $videoInfo['format'] = (string)$track->Format;
                $videoInfo['formatInfo'] = (string)$track->Format_Info;
                $videoInfo['formatProfile'] = (string)$track->Format_profile;
                $videoInfo['formatSettingsCABAC'] = (string)$track->Format_settings__CABAC;
                $videoInfo['formatSettingsReFrames'] = (string)$track->Format_settings__ReFrames;
                $videoInfo['codecId'] = (string)$track->Codec_ID;
                $videoInfo['codecIdInfo'] = (string)$track->Codec_ID_Info;
                $videoInfo['duration'] = $this->getSeconds($track->Duration);
                $videoInfo['bitRate'] = $this->getBitsPerSecond($track->Bit_rate);
                $videoInfo['maximumBitRate'] = $this->getBitsPerSecond($track->Maximum_bit_rate);
                $videoInfo['width'] = (int)preg_replace('/\s/', '', $track->Width);
                $videoInfo['height'] = (int)preg_replace('/\s/', '', $track->Height);
                $videoInfo['displayAspectRatio'] = (string)$track->Display_aspect_ratio;
                $videoInfo['frameRateMode'] = (string)$track->Frame_rate_mode;
                $videoInfo['frameRate'] = (string)$track->Frame_rate;
                $videoInfo['colorSpace'] = (string)$track->Color_space;
                $videoInfo['chromaSubSampling'] = (string)$track->Chroma_subsampling;
                $videoInfo['bitDepth'] = (string)$track->Bit_depth;
                $videoInfo['scanType'] = (string)$track->Scan_type;
                $videoInfo['bitsPixelFrame'] = (string)$track->Bits__Pixel_Frame_;
                $videoInfo['streamSize'] = (int) $this->getFileSize($track->Stream_size);
				if (isset($track->Original_width)) {
					$videoInfo['width'] = (int)preg_replace('/\s/', '', $track->Original_width);
				}
				if (isset($track->Original_height)) {
					$videoInfo['height'] = (int)preg_replace('/\s/', '', $track->Original_height);
				}
            }

            if ($type == 'Audio') {
                $audioInfo['id'] = (string)$track->ID;
                $audioInfo['format'] = (string)$track->Format;
                $audioInfo['formatInfo'] = (string)$track->Format_Info;
                $audioInfo['formatProfile'] = (string)$track->Format_profile;
                $audioInfo['codecId'] = (string)$track->Codec_ID;
                $audioInfo['duration'] = $this->getSeconds($track->Duration);
                $audioInfo['bitRateMode'] = (string)$track->Bit_rate_mode;
                $audioInfo['bitRate'] = $this->getBitsPerSecond($track->Bit_rate);
                $audioInfo['maximumBitRate'] = $this->getBitsPerSecond($track->Maximum_bit_rate);
                $audioInfo['channels'] = (string)$track->Channel_s_;
                $audioInfo['channelPositions'] = (string)$track->Channel_positions;
                $audioInfo['samplingRate'] = (string)$track->Sampling_rate;
                $audioInfo['compressionMode'] = (string)$track->Compression_mode;
                $audioInfo['streamSize'] = $this->getFileSize($track->Stream_size);
            }
        }

        $fileInfo['generalInfo'] = $generalInfo;
        $fileInfo['videoInfo'] = $videoInfo;
        $fileInfo['audioInfo'] = $audioInfo;

        return $fileInfo;
    }

    /**
     * Executes the command to obtain the media info
     *
     * @param string $filePath
     * @return array
     * @throws TimeoutException
     */
	protected function obtainMediaInfo($filePath)
	{
		$xmlOutput = array();
		$returnValue = 0;
		exec('timeout 50 mediainfo --Output=XML '.$filePath, $xmlOutput,$returnValue);
		if($returnValue===124) {
            throw new TimeoutException("Mediainfo has timed out...");
        }
		else {
		    return $xmlOutput;
        }
	}

	/**
	 * Gets the file size in byes from the file size returned by media info
	 *
	 * @param $value (xxx bytes|Kib|Mib|Gib|Tib)
	 *
	 * @return int
	 */
	public function getFileSize($value)
	{
		if (null == $value || strlen(trim($value)) < 1) {
			return null;
		}

		$data = explode(' ', $value);

		$size = (float) $data[0];

		if (!isset($data[1])) {
			return $size;
		}

		$measure = strtolower($data[1]);

		if ($measure == 'bytes') {
			return $size;
		}

		if ($measure == 'kib') {
			return $size * 1024;
		}

		if ($measure == 'mib') {
			return $size * 1024 * 1024;
		}

		if ($measure == 'gib') {
			return $size * 1024 * 1024 * 1024;
		}

		if ($measure == 'tib') {
			return $size * 1024 * 1024 * 1024 * 1024;
		}

		return $size;
	}

	/**
	 * Gets the media duration in seconds from the media info Duration variable
	 *
	 * @param $value (xxh xxmn  xxs xxxms)
	 *
	 * @return int Seconds format
	 */
	public function getSeconds($value)
	{

		if (null == $value || strlen(trim($value)) < 1) {
			return null;
		}

        $patterns = array ('/(\s*)(\d+)(\s*)(\w+)/');
        $replace = array ('\2\4 ');
        $data = preg_replace($patterns,$replace,$value);
        $data = explode(' ', $data);
		$seconds = 0;

		foreach ($data as $item) {
			$value = (float)$item;
			$measure = strtolower(str_replace($value, '', $item));

			if ($measure === 'h') {
				$seconds += $value * 3600;
				continue;
			}

			if ($measure === 'mn' || $measure === 'min') {
				$seconds += $value * 60;
				continue;
			}

			if ($measure === 's') {
				$seconds += $value;
				continue;
			}
		}

		return $seconds;
	}

	/**
	 * Gets the bit rate in Bits Per Second returned from the media info command
	 *
	 * @param $data ( xxxx bps|kbps|mbps|tbps)
	 *
	 * @return int Kbps format
	 */
	public function getBitsPerSecond($data)
	{
		if (null == $data || strlen(trim($data)) < 1) {
			return null;
		}

		$data  = preg_replace('/\s/', '', $data);

		$value = (float)$data;
		$measure = strtolower(str_replace($value, '', $data));

		if ($measure === 'bps' || $measure === 'b/s') {
			return $value;
		}

		if ($measure === 'kbps' || $measure === 'kb/s') {
			return $value * 1000;
		}

		if ($measure === 'mbps' || $measure === 'mb/s') {
			return $value * 1000 * 1000;
		}

		if ($measure === 'gbps' || $measure === 'gb/s') {
			return $value * 1000 * 1000 * 1000;
		}

		if ($measure === 'tbps' || $measure === 'tb/s') {
			return $value * 1000 * 1000 * 1000 * 1000;
		}

		return $value;
	}

}